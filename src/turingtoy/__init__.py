from typing import Dict, Optional, Tuple, List


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List[Dict], bool]:
    # Décomposition de la machine de Turing
    blank = machine["blank"]
    start_state = machine["start state"]
    final_states = machine["final states"]
    table = machine["table"]

    # Initialisation de l'état de la machine
    tape = [blank] * 100  # Ruban de taille arbitraire pour la simulation
    tape[1 : len(input_) + 1] = list(input_)
    position = 1
    state = start_state
    history: List[Dict] = []

    # Fonction utilitaire pour créer un dictionnaire d'instruction à partir d'une liste d'états
    def to_dict(states: List[str], direction: str) -> Dict[str, Dict]:
        return {symbol: {direction: state} for symbol in states}

    # Exécution de la machine de Turing
    step = 0
    while steps is None or step < steps:
        symbol = tape[position]

        # Vérification de l'arrêt dans un état final
        if state in final_states:
            return (
                "".join(tape).strip(blank),
                history,
                True,
            )

        # Recherche de l'instruction correspondante dans la table de transition
        if symbol in table[state]:
            instruction = table[state][symbol]
        else:
            return (
                "".join(tape).strip(blank),
                history,
                False,
            )

        # Traitement de l'instruction
        if isinstance(instruction, str):
            # Déplacement simple sans écriture
            direction = instruction
            next_state = state
            write_symbol = None
        else:
            # Déplacement et écriture
            write_symbol = instruction.get("write")
            direction = instruction.get("R") or instruction.get("L")
            next_state = instruction.get(direction)

        # Mise à jour de l'historique
        history.append(
            {
                "state": state,
                "reading": symbol,
                "position": position,
                "memory": tape.copy(),
                "transition": instruction,
            }
        )

        # Écriture du symbole
        if write_symbol is not None:
            tape[position] = write_symbol

        # Déplacement de la tête de lecture
        if direction == "R":
            position += 1
        elif direction == "L":
            position -= 1

        state = next_state
        step += 1

    return (
        "".join(tape).strip(blank),
        history,
        False,
    )
